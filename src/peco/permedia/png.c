/*
	libpermedia -- Perun multimedia library

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <perun_abi.h>
#include <png.h>

extern Per_Object* libpermedia_makeImage(int width, int height, char *data);

Per_Object* libpermedia_fromPNG(const char *filename)
{
	unsigned char header[8];
	
	FILE *fp = fopen(filename, "rb");
	if (fp == NULL)
	{
		return NULL;
	};
	
	fread(header, 1, 8, fp);
	if (png_sig_cmp(header, 0, 8))
	{
		fclose(fp);
		return NULL;
	};
	
	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
	{
		fclose(fp);
		return NULL;
	};
	
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		fclose(fp);
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return NULL;
	};
	
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fclose(fp);
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return NULL;
	};
	
	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, 8);
	png_read_info(png_ptr, info_ptr);
	
	int width = png_get_image_width(png_ptr, info_ptr);
	int height = png_get_image_height(png_ptr, info_ptr);
	png_byte colorType = png_get_color_type(png_ptr, info_ptr);
	
	png_read_update_info(png_ptr, info_ptr);
	
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fclose(fp);
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return NULL;
	};
	
	png_bytep *rowPointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
	int y;
	for (y=0; y<height; y++)
	{
		rowPointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr, info_ptr));
	};
	
	png_read_image(png_ptr, rowPointers);
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	fclose(fp);
	
	if (colorType & PNG_COLOR_MASK_PALETTE)
	{
		for (y=0; y<height; y++)
		{
			free(rowPointers[y]);
		};
		
		free(rowPointers);
		return NULL;
	};
	
	int bpp;
	if (colorType & PNG_COLOR_MASK_COLOR)
	{
		bpp = 3;
	}
	else
	{
		bpp = 1;
	};
	
	if (colorType & PNG_COLOR_MASK_ALPHA)
	{
		bpp++;
	};
	
	char *data = (char*) Per_NewArray(4 * width * height, 1, PER_TYPESECT_PRIMITIVE);
	for (y=0; y<height; y++)
	{
		int x;
		for (x=0; x<width; x++)
		{
			char *put = &data[4 * (y * width + x)];
			png_byte *fetch = &rowPointers[y][bpp * x];
			
			if (colorType & PNG_COLOR_MASK_COLOR)
			{
				put[0] = fetch[0];
				put[1] = fetch[1];
				put[2] = fetch[2];
			}
			else
			{
				put[0] = put[1] = put[2] = fetch[0];
			};
			
			if (colorType & PNG_COLOR_MASK_ALPHA)
			{
				put[3] = fetch[bpp-1];
			}
			else
			{
				put[3] = 0xFF;
			};
		};
	};
	
	for (y=0; y<height; y++)
	{
		free(rowPointers[y]);
	};
	
	free(rowPointers);
	Per_Object *result = libpermedia_makeImage(width, height, data);
	Per_ArrayDown(data);
	return result;
};

int libpermedia_toPNG(const char *filename, int width, int height, char *data)
{
	FILE *fp = fopen(filename, "wb");
	if (fp == NULL)
	{
		return -1;
	};
	
	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
	{
		fclose(fp);
		return -1;
	};
	
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		fclose(fp);
		png_destroy_write_struct(&png_ptr, NULL);
		return -1;
	};

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fclose(fp);
		png_destroy_write_struct(&png_ptr, &info_ptr);
		return -1;
	};
	
	png_init_io(png_ptr, fp);
	
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fclose(fp);
		png_destroy_write_struct(&png_ptr, &info_ptr);
		return -1;
	};
	
	png_set_IHDR(png_ptr, info_ptr, width, height,
			8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
			PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	
	
	png_write_info(png_ptr, info_ptr);
	
	png_bytep *rowPointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
	size_t y;
	for (y=0; y<height; y++)
	{
		rowPointers[y] = (png_byte*) &data[4 * y * width];
	};
	
	png_write_image(png_ptr, rowPointers);
	png_write_end(png_ptr, NULL);
	png_destroy_write_struct(&png_ptr, &info_ptr);
	free(rowPointers);
	fclose(fp);
	
	return 0;
};
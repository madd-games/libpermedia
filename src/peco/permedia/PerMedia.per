/*
	libpermedia -- Perun multimedia library

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace peco.permedia;

import peco.permedia.ll.SDL;

/**
 * Some common routines as well as initialization.
 */
public static class PerMedia
{
	/**
	 * The NULL pointer from C.
	 */
	public static final ptr_t nullptr = (ptr_t) 0;
	
	private static int glProfile = -1;
	private static int glVersionMajor = 1;
	private static int glVersionMinor = 1;
	
	// the SDL window and OpenGL context
	private static ptr_t sdlWindow;
	private static ptr_t glContext;
	
	private static Event processEvent(int[] eventData)
	{
		// translate an SDL event (SDL_Event) to an Event object.
		int eventType = eventData[0];
		if (eventType == SDL.QUIT)
		{
			return Event.makeCloseEvent();
		}
		else if (eventType == SDL.MOUSEMOTION)
		{
			return Event.makeMouseMotionEvent(eventData[5], eventData[6]);
		}
		else if (eventType == SDL.MOUSEBUTTONDOWN)
		{
			return Event.makeMouseButtonDownEvent(eventData[5], eventData[6], eventData[4] & 0xFF);
		}
		else if (eventType == SDL.MOUSEBUTTONUP)
		{
			return Event.makeMouseButtonUpEvent(eventData[5], eventData[6], eventData[4] & 0xFF);
		}
		else if (eventType == SDL.KEYDOWN)
		{
			return Event.makeKeyDownEvent(eventData[5]);
		}
		else if (eventType == SDL.KEYUP)
		{
			return Event.makeKeyUpEvent(eventData[5]);
		}
		else
		{
			// not yet recognised
			return Event.makeUnknownEvent();
		};
	};
	
	/**
	 * Set the requested OpenGL profile.
	 * 
	 * Call this function before calling `createWindow()`. By default, a compatibility profile
	 * is created.
	 * 
	 * \param profile One of the `GLProfile` values.
	 */
	public static void setGLProfile(GLProfile profile)
	{
		if (profile == GLProfile.COMPATIBILITY)
		{
			glProfile = SDL.GL_CONTEXT_PROFILE_COMPATIBILITY;
		}
		else if (profile == GLProfile.CORE)
		{
			glProfile = SDL.GL_CONTEXT_PROFILE_CORE;
		}
		else if (profile == GLProfile.ES)
		{
			glProfile = SDL.GL_CONTEXT_PROFILE_ES;
		}
		else
		{
			throw new IllegalArgumentError("invalid enum; this is probably a bug!");
		};
	};
	
	/**
	 * Set the requested OpenGL version.
	 * 
	 * Call this function before calling `createWindow()`. By default, OpenGL 1.1 is requested.
	 * 
	 * \param major The major version number.
	 * \param minor The minor version number.
	 */
	public static void setGLVersion(int major, int minor)
	{
		glVersionMajor = major;
		glVersionMinor = minor;
	};
	
	/**
	 * Create an OpenGL window.
	 * 
	 * You should call the `setGL*()` functions before calling this one. Furthermore, this function must
	 * only be called ONCE, before calling any other window or OpenGL functions. This is because it also
	 * performs some initialization.
	 * 
	 * The new window will have an OpenGL context. The parameters of this context must have been previously
	 * set by the `setGL*()` functions; see their documentation for default values.
	 * 
	 * \param caption The window caption (displayed in the title bar, etc).
	 * \param width The width of the window's client area, in pixels.
	 * \param height The height of the window's client area, in pixels.
	 * 
	 * \throws peco.permedia.PerMediaError If the window creation, context creation or other type of initialization
	 *                                     failed.
	 */
	public static void createWindow(String caption, int width, int height)
	{
		if (glProfile == -1)
		{
			glProfile = SDL.GL_CONTEXT_PROFILE_COMPATIBILITY;
		};
		
		if (SDL.Init(SDL.INIT_VIDEO) != 0)
		{
			throw new PerMediaError("failed to initialize SDL");
		};
		
		sdlWindow = SDL.CreateWindow(
			caption.toByteArray(),
			SDL.WINDOWPOS_CENTERED, SDL.WINDOWPOS_CENTERED,
			width, height,
			SDL.WINDOW_SHOWN | SDL.WINDOW_OPENGL);
		if (sdlWindow == nullptr)
		{
			SDL.Quit();
			
			throw new PerMediaError("failed to create window");
		};
		
		SDL.GL_SetAttribute(SDL.GL_CONTEXT_MAJOR_VERSION, glVersionMajor);
		SDL.GL_SetAttribute(SDL.GL_CONTEXT_MINOR_VERSION, glVersionMinor);
		SDL.GL_SetAttribute(SDL.GL_CONTEXT_PROFILE_MASK, glProfile);
		
		glContext = SDL.GL_CreateContext(sdlWindow);
		if (glContext == nullptr)
		{
			SDL.DestroyWindow(sdlWindow);
			SDL.Quit();
			
			throw new PerMediaError("failed to create an OpenGL context");
		};
		
		GL._init();
	};
	
	/**
	 * Present the OpenGL drawings to the window.
	 * 
	 * Displays the current OpenGL drawing to the window.
	 */
	public static void present()
	{
		SDL.GL_SwapWindow(sdlWindow);
	};
	
	/**
	 * Return a pending event if one exists.
	 * 
	 * This function takes the next pending event and removes it from the queue, and
	 * returns it.
	 * 
	 * \param wait If `true`, this function will wait for an event if there isn't one;
	 *             otherwise, returns `null` if there is no event.
	 * \returns An `Event` object if an event was pending, `null` if not.
	 */
	public static Event getEvent(bool wait)
	{
		int[] eventData = new int[14];
		int status;
		
		if (wait)
		{
			status = SDL.WaitEvent(eventData);
		}
		else
		{
			status = SDL.PollEvent(eventData);
		};
		
		if (status == 0)
		{
			return null;
		};
		
		return processEvent(eventData);
	};

	/**
	 * Return a pending event if one exists.
	 * 
	 * This function takes the next pending event and removes it from the queue, and
	 * returns it. It returns `null` if no event is pending.
	 * 
	 * \returns An `Event` object if an event was pending, `null` if not.
	 */
	public static Event getEvent()
	{
		return getEvent(false);
	};
	
	/**
	 * Destroy the window.
	 * 
	 * This destroys the window, OpenGL context, etc, and quits SDL. You can create a new window
	 * after that if you want.
	 */
	public static void destroyWindow()
	{
		SDL.GL_DeleteContext(glContext);
		SDL.DestroyWindow(sdlWindow);
		SDL.Quit();
		
		sdlWindow = nullptr;
		glContext = nullptr;
	};
	
	/**
	 * Returns `true` if the specified OpenGL extension is supported.
	 * 
	 * You can only call this function AFTER the window has been created!
	 * 
	 * \param name The name of the extension to check for.
	 * \returns `true` if the specified extension is supported in the current context.
	 */
	public static bool isGLExtensionSupported(String name)
	{
		if (sdlWindow == nullptr)
		{
			throw new PerMediaError("isGLExtensionSupported() can only be called after the window is created!");
		};
		
		return SDL.GL_ExtensionSupported(name.toByteArray()) != 0;
	};
	
	/**
	 * Returns the number of milliseconds ellapsed since some arbitrary point in time.
	 */
	public static int getTicks()
	{
		return SDL.GetTicks();
	};
	
	/**
	 * Move the mouse cursor to the specified position.
	 * 
	 * The coordinates are relative to the window, which must already have been created with
	 * `createWindow()`.
	 */
	public static void moveMouse(int x, int y)
	{
		SDL.WarpMouseInWindow(sdlWindow, x, y);
	};
	
	/**
	 * Set whether to show the mouse cursor.
	 */
	public static void setCursorVisible(bool visible)
	{
		if (visible)
		{
			SDL.ShowCursor(1);
		}
		else
		{
			SDL.ShowCursor(0);
		};
	};
};
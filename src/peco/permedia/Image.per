/*
	libpermedia -- Perun multimedia library

	Copyright (c) 2020, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace peco.permedia;

/**
 * A bitmap image.
 * 
 * This class stores a 2D grid of RGBA color values.
 */
public final class Image
{
	private int width;
	private int height;
	private byte_t[] data;
	
	private Image() {};
	
	// in png.c
	protected static Image _fromPNG(byte_t[] filename) extern libpermedia_fromPNG;
	protected static int _toPNG(byte_t[] filename, int width, int height, byte_t[] data) extern libpermedia_toPNG;
	
	protected static Image makeImage(int width, int height, byte_t[] data) extern libpermedia_makeImage
	{
		return new Image(width, height, data);
	};
	
	/**
	 * Create an image of the specified dimensions, initially completly transparent.
	 */
	public Image(int width, int height)
	{
		if (width < 0 || height < 0)
		{
			throw new IllegalArgumentError("negative image dimensions");
		};
		
		this.width = width;
		this.height = height;
		this.data = new byte_t[4 * width * height];
	};
	
	/**
	 * Create an image with the specified data and dimensions.
	 * 
	 * See `getData()` for an explanation of the image data format.
	 * 
	 * \throws std.rt.IllegalArgumentError If `data` is the wrong size, or dimensions are negative.
	 */
	public Image(int width, int height, byte_t[] data)
	{
		if (width < 0 || height < 0)
		{
			throw new IllegalArgumentError("negative image dimensions");
		};
		
		if (data.length != 4 * width * height)
		{
			throw new IllegalArgumentError("data is of the incorrect size");
		};
		
		this.width = width;
		this.height = height;
		this.data = data;
	};
	
	/**
	 * Get the width of this image.
	 */
	public int getWidth()
	{
		return width;
	};
	
	/**
	 * Get the height of this image.
	 */
	public int getHeight()
	{
		return height;
	};
	
	/**
	 * Returns the image data.
	 * 
	 * The returned array is a left-right, top-down array of pixels, with each pixel being in RGBA
	 * format. You can modify this array to change pixels in the image.
	 */
	public byte_t[] getData()
	{
		return data;
	};
	
	/**
	 * Load an image from a PNG file.
	 * 
	 * \throws peco.permedia.PerMediaError If the image cannot be loaded.
	 */
	public static Image fromPNG(String filename)
	{
		Image img = _fromPNG(filename.toByteArrayWithTerminator());
		if (img == null)
		{
			throw new PerMediaError("could not load PNG image: " + filename);
		};
		
		return img;
	};
	
	/**
	 * Write the image to a PNG file.
	 * 
	 * \throws peco.permedia.PerMediaError If the image cannot be saved.
	 */
	public void toPNG(String filename)
	{
		if (_toPNG(filename.toByteArrayWithTerminator(), width, height, data) != 0)
		{
			throw new PerMediaError("could not write PNG image: " + filename);
		};
	};
	
	/**
	 * Overlay another image on top of this one.
	 * 
	 * Takes a width*height crop of the specified image starting at (srcX, srcY), and overlays
	 * it on this one at (destX, destY). This method DOES NOT perform alpha blending: it simply
	 * copies the data, including any alpha channel values.
	 * 
	 * \throws std.rt.IllegalArgumentError If any of the coordinates are negative.
	 */
	public void overlay(int destX, int destY, Image src, int srcX, int srcY, int width, int height)
	{
		if (srcX < 0 || srcY < 0 || destX < 0 || destY < 0)
		{
			throw new IllegalArgumentError("negative coordinates");
		};
		
		int actualWidth = width;
		int actualHeight = height;
		
		if (srcX + actualWidth > src.width)
		{
			actualWidth = src.width - srcX;
		};
		
		if (srcY + actualHeight > src.height)
		{
			actualHeight = src.height - srcY;
		};
		
		if (destX + actualWidth > this.width)
		{
			actualWidth = this.width - destX;
		};
		
		if (destY + actualHeight > this.height)
		{
			actualHeight = this.height - destY;
		};
		
		if (actualWidth < 1 || actualHeight < 1)
		{
			// nothing to be done
			return;
		};
		
		int plotY;
		for (plotY=0; plotY<actualHeight; plotY++)
		{
			int srcStart = 4 * ((srcY+plotY) * src.width + srcX);
			int destStart = 4 * ((destY+plotY) * this.width + destX);
			
			int i;
			for (i=0; i<actualWidth*4; i++)
			{
				data[destStart+i] = src.data[srcStart+i];
			};
		};
	};
	
	/**
	 * Fill a rectangle in this image with the specified color.
	 * 
	 * This method does NOT perform any alpha blending; the pixels are simply
	 * replaced with the specified color, including the alpha channel.
	 * 
	 * \throws std.rt.IllegalArgumentError If the coordinates are negative.
	 */
	public void fillRect(int x, int y, int width, int height, byte_t red, byte_t green, byte_t blue, byte_t alpha)
	{
		if (x < 0 || y < 0)
		{
			throw new IllegalArgumentError("negative coordinates");
		};
		
		int actualWidth = width;
		int actualHeight = height;
		
		if (x + actualWidth > this.width)
		{
			actualWidth = this.width - x;
		};
		
		if (y + actualHeight > this.height)
		{
			actualHeight = this.height - y;
		};
		
		int plotX;
		int plotY;
		
		for (plotY=y; plotY<y+actualHeight; plotY++)
		{
			for (plotX=x; plotX<x+actualWidth; plotX++)
			{
				int pos = 4 * (plotY * this.width + plotX);
				data[pos+0] = red;
				data[pos+1] = green;
				data[pos+2] = blue;
				data[pos+3] = alpha;
			};
		};
	};
};
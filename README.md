# PreMedia - Perun multimedia library

PerMedia is a _WORK IN PROGRESS_ multimedia library for Perun. The planned features are:

- An interface to OpenGL (including extensions), including window and context management.
- Image manipulation (loading/saving at least PNG, easy way to load OpenGL textures etc).
- OpenAL support for 3D audio.
- Standard audio too.

The library is designed to be cross-platform. It uses SDL2 under the hood.

See the `demos` directory for some examples.